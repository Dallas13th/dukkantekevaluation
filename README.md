# README #

# The task:
```
Evaluation

We have an inventory with :
products(name, barcode, description, weight, status(sold, inStock, damaged))
categories (name)

Using DDD, SQL server, EF Core for the database provider, .Net core, no UI needed just the APIs
Create 3 APIs
1) Count the number of products sold, damaged and inStock
2) Change the status of a product
3) Sell a product
Please get back to us with the response within 48 hrs of receiving this e-mail.
```

# For project compilation make sure you have installed:
* The Latest [Visual studio 2022](https://visualstudio.microsoft.com/downloads/)
* The Latest [.Net Framework SDK](https://www.microsoft.com/net/download/visual-studio-sdks)

# Getting Started:
- In DukkantekApi project copy and rename file
```
appsettings-template.json
```
to
```
appsettings.json
```

- Specify connection string in format:
```
"Server=SERVERNAME;Database=DBNAME;User Id=USERNAME;Password=SECRET"
```

- Run the command in terminal:
```
dotnet ef database update --context DukkantekApi.DataAccess.Databases.Products.ProductsDbContextBase --project DukkantekApi.DataAccess/DukkantekApi.DataAccess.csproj --startup-project DukkantekApi --connection "Server=SERVERNAME;Database=DBNAME;User Id=USERNAME;Password=SECRET"
```