﻿namespace DukkantekApi.Domain.Models.Errors
{
    public class ApiResponseErrorModel
    {
        public Guid ErrorGuid { get; } = Guid.NewGuid();

        public string Message { get; set; } = null!;

        public ErrorSystemInfo? SystemInfo { get; set; }

        public ApiResponseErrorModel? InnerError { get; set; }
        
        public List<ValidationErrorModel> ValidationErrors { get; set; } = null!;
    }
}