﻿namespace DukkantekApi.Domain.Models.Errors
{
    public class ErrorSystemInfo
    {
        public string Type { get; set; } = null!;

        public string Message { get; set; } = null!;

        public string StackTrace { get; set; } = null!;
    }
}