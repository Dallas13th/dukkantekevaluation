﻿namespace DukkantekApi.Domain.Models.Errors
{
    public class ValidationErrorModel
    {
        public string Key { get; set; } = string.Empty;

        public string ValidationMessage { get; set; } = string.Empty;
    }
}