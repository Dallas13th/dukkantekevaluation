﻿namespace DukkantekApi.Domain.Models.Products
{
    public sealed class StatusResultModel
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;
    }
}