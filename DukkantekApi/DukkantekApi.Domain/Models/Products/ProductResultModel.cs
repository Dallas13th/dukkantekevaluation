﻿namespace DukkantekApi.Domain.Models.Products
{
    public sealed class ProductResultModel
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Barcode { get; set; } = string.Empty;

        public string ProductDescription { get; set; } = string.Empty;

        public decimal Weight { get; set; }

        public string StatusName { get; set; } = string.Empty;

        public string CategoryName { get; set; } = string.Empty;
    }
}