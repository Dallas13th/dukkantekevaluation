﻿using DukkantekApi.DataAccess.Databases.Products;
using DukkantekApi.Domain.Models.Products;
using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.Domain.Services
{
    public sealed class StatusService : IStatusService
    {
        private readonly ProductsDbContextBase _productsContext;

        public StatusService(ProductsDbContextBase productsContext)
        {
            _productsContext = productsContext;
        }

        public async Task<List<StatusResultModel>> GetStatusesAsync()
        {
            var statuses = _productsContext.Statuses
                .Select(s => new StatusResultModel
                {
                    Id = s.Id,
                    Name = s.Name,
                });

            return await statuses.ToListAsync();
        }
    }
}