﻿using DukkantekApi.Domain.Models.Products;

namespace DukkantekApi.Domain.Services
{
    public interface IProductsService
    {
        public Task<List<ProductResultModel>> GetProductsListAsync(int? statusId);

        public Task<int> GetProductsCountAsync(int? statusId);

        public Task SetProductStatusAsync(int productId, int statusId);

        public Task SellProductAsync(int productId);
    }
}