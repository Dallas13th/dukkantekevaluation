﻿using DukkantekApi.DataAccess.Databases.Products;
using DukkantekApi.DataAccess.Databases.Products.Entities;
using DukkantekApi.Domain.Exceptions;
using DukkantekApi.Domain.Models.Errors;
using DukkantekApi.Domain.Models.Products;
using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.Domain.Services
{
    public sealed class ProductsService : IProductsService
    {
        private readonly ProductsDbContextBase _productsContext;

        public ProductsService(ProductsDbContextBase productsContext)
        {
            _productsContext = productsContext;
        }

        public async Task<List<ProductResultModel>> GetProductsListAsync(int? statusId)
        {
            var products = _productsContext.Products;

            var filtered = statusId.HasValue
                ? products.Where(w => w.StatusId == statusId)
                : products;

            var result = filtered
                .Select(s => new ProductResultModel
                {
                    Id = s.Id,
                    Name = s.Name,
                    Barcode = s.Barcode,
                    ProductDescription = s.ProductDescription,
                    Weight = s.Weight,
                    StatusName = s.Status.Name,
                    CategoryName = s.Category.Name
                });

            return await result.ToListAsync();
        }
        
        public async Task<int> GetProductsCountAsync(int? statusId)
        {
            var products = _productsContext.Products;

            var filtered = statusId.HasValue
                ? products.Where(w => w.StatusId == statusId)
                : products;

            var count = await filtered.CountAsync();

            return count;
        }

        public async Task SetProductStatusAsync(int productId, int statusId)
        {
            var product = await _productsContext.Products.FirstOrDefaultAsync(w => w.Id == productId);
            var status = await _productsContext.Statuses.FirstOrDefaultAsync(w => w.Id == statusId);

            var validationErrors = new List<ValidationErrorModel>();

            if (product == null)
            {
                validationErrors.Add(new ValidationErrorModel
                {
                    Key = nameof(productId),
                    ValidationMessage = $"Product with id {productId} does not exist"
                });
            }
            
            if (status == null)
            {
                validationErrors.Add(new ValidationErrorModel
                {
                    Key = nameof(statusId),
                    ValidationMessage = $"Status with id {statusId} does not exist"
                });
            }

            if (validationErrors.Any())
                throw new DomainValidationException("Some of arguments are invalid", validationErrors);

            product!.StatusId = statusId;

            await _productsContext.SaveChangesAsync();
        }

        public async Task SellProductAsync(int productId)
        {
            await SetProductStatusAsync(productId, Status.SoldId);
        }
    }
}