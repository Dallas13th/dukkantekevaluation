﻿using DukkantekApi.Domain.Models.Products;

namespace DukkantekApi.Domain.Services
{
    public interface IStatusService
    {
        public Task<List<StatusResultModel>> GetStatusesAsync();
    }
}