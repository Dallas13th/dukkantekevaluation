﻿using DukkantekApi.Domain.Models.Errors;

namespace DukkantekApi.Domain.Exceptions
{
    public class DomainValidationException : Exception
    {
        public DomainValidationException(string message)
            : base(message)
        {

        }

        public DomainValidationException(string message, Exception innerException)
            : base(message, innerException)
        {

        }

        public DomainValidationException(string message, List<ValidationErrorModel> validationErrors)
            : base(message)
        {
            ValidationErrors = validationErrors;
        }

        public DomainValidationException(string message, Exception innerException, List<ValidationErrorModel> validationErrors)
            : base(message, innerException)
        {
            ValidationErrors = validationErrors;
        }

        public List<ValidationErrorModel> ValidationErrors { get; set; } = new();
    }
}