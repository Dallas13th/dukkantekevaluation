﻿namespace DukkantekApi.DataAccess.Databases
{
    public interface IDbConnectionSettings
    {
        public string ConnectionString { get; set; }
    }
}