﻿namespace DukkantekApi.DataAccess.Databases
{
    public class BasicDbConnectionSettings : IDbConnectionSettings
    {
        public BasicDbConnectionSettings(string connectionString)
        {
            ConnectionString = connectionString;
        }

        public string ConnectionString { get; set; }
    }
}