﻿using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.DataAccess.Databases.Products
{
    public class ProductsSqlServerContext : ProductsDbContextBase
    {
        private readonly IDbConnectionSettings _settings;
        
        public ProductsSqlServerContext(IDbConnectionSettings settings)
        {
            _settings = settings;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_settings.ConnectionString);
        }
    }
}