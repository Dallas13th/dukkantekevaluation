﻿using DukkantekApi.DataAccess.Databases.Products.Entities;
using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.DataAccess.Databases.Products.ContentBuilders
{
    internal static class ProductBuilder
    {
        public static void FillProducts(this ModelBuilder modelBuilder)
        {
            var products = Enumerable.Range(1, 100)
                .Select(s => new Product
                {
                    Id = s,
                    Name = $"Product name mock {s}",
                    Barcode = $"https://product-barcode-link-content.example/prouct/{s}",
                    ProductDescription = $"This is a product that generated to be content mock {s}",
                    Weight = 42,
                    CategoryId = (s % 5) + 1,
                    StatusId = Status.InStockId
                })
                .ToArray();

            modelBuilder.Entity<Product>()
                .HasData(products);
        }
    }
}