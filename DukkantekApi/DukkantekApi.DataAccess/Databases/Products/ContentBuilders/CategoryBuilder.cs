﻿using DukkantekApi.DataAccess.Databases.Products.Entities;
using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.DataAccess.Databases.Products.ContentBuilders
{
    internal static class CategoryBuilder
    {
        public static void FillCategories(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>()
                .HasData(new[]
                {
                    new Category {Id = 1, Name = "Unspecified"},
                    new Category {Id = 2, Name = "Medicine"},
                    new Category {Id = 3, Name = "Electronics"},
                    new Category {Id = 4, Name = "Food and drink"},
                    new Category {Id = 5, Name = "Clothes"}
                });
        }
    }
}