﻿using DukkantekApi.DataAccess.Databases.Products.Entities;
using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.DataAccess.Databases.Products.ContentBuilders
{
    internal static class StatusBuilder
    {
        public static void FillStatuses(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Status>()
                .HasData(new[]
                {
                    new Status {Id = 1, Name = "inStock"},
                    new Status {Id = 2, Name = "sold"},
                    new Status {Id = 3, Name = "damaged"}
                });
        }
    }
}