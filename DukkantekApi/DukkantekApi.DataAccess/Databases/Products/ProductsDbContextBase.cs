﻿using DukkantekApi.DataAccess.Databases.Products.ContentBuilders;
using DukkantekApi.DataAccess.Databases.Products.Entities;
using Microsoft.EntityFrameworkCore;

namespace DukkantekApi.DataAccess.Databases.Products
{
    public class ProductsDbContextBase : DbContext
    {
        protected ProductsDbContextBase()
        {

        }

        protected ProductsDbContextBase(DbContextOptions<ProductsDbContextBase> options)
            : base(options)
        {

        }

        public virtual DbSet<Product> Products { get; set; } = null!;

        public virtual DbSet<Category> Categories { get; set; } = null!;

        public virtual DbSet<Status> Statuses { get; set; } = null!;
        
        //Configure DbSchema only
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Category>(entity =>
            {
                entity.ToTable("Category");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("Status");
                entity.HasKey(e => e.Id);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100);
            });


            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("Product");
                entity.HasKey(e => e.Id);
                
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode();

                entity.Property(e => e.Barcode)
                    .IsRequired()
                    .HasMaxLength(1000)
                    .IsUnicode();

                entity.Property(e => e.ProductDescription)
                    .IsRequired()
                    .IsUnicode();

                entity.Property(e => e.Weight)
                    .HasColumnType("DECIMAL")
                    .HasPrecision(12, 9);
                
                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Product_StatusId__Status_Id");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Products)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_Product_CategoryId__Category_Id");
            });

            InsertDefaultContent(modelBuilder);
        }

        //Configure content only
        private void InsertDefaultContent(ModelBuilder modelBuilder)
        {
            modelBuilder.FillCategories();
            modelBuilder.FillStatuses();

            modelBuilder.FillProducts();
        }
    }
}
