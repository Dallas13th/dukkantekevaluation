﻿namespace DukkantekApi.DataAccess.Databases.Products.Entities
{
    public class Status
    {
        public const int InStockId = 1;
        public const int SoldId = 2;

        public Status()
        {
            Products = new HashSet<Product>();
        }

        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public ICollection<Product> Products { get; set; }
    }
}