﻿namespace DukkantekApi.DataAccess.Databases.Products.Entities
{
    public class Product
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Barcode { get; set; } = string.Empty;

        public string ProductDescription { get; set; } = string.Empty;

        public decimal Weight { get; set; }

        public int CategoryId { get; set; }

        public int StatusId { get; set; }

        public Category Category { get; set; } = null!;

        public Status Status { get; set; } = null!;
    }
}