﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DukkantekApi.DataAccess.Databases.Products.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Category",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Status",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Status", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Product",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false),
                    Barcode = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: false),
                    ProductDescription = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Weight = table.Column<decimal>(type: "DECIMAL(12,9)", precision: 12, scale: 9, nullable: false),
                    CategoryId = table.Column<int>(type: "int", nullable: false),
                    StatusId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Product", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Product_CategoryId__Category_Id",
                        column: x => x.CategoryId,
                        principalTable: "Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Product_StatusId__Status_Id",
                        column: x => x.StatusId,
                        principalTable: "Status",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Category",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Unspecified" },
                    { 2, "Medicine" },
                    { 3, "Electronics" },
                    { 4, "Food and drink" },
                    { 5, "Clothes" }
                });

            migrationBuilder.InsertData(
                table: "Status",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "inStock" },
                    { 2, "sold" },
                    { 3, "damaged" }
                });

            migrationBuilder.InsertData(
                table: "Product",
                columns: new[] { "Id", "Barcode", "CategoryId", "Name", "ProductDescription", "StatusId", "Weight" },
                values: new object[,]
                {
                    { 1, "https://product-barcode-link-content.example/prouct/1", 2, "Product name mock 1", "This is a product that generated to be content mock 1", 1, 42m },
                    { 2, "https://product-barcode-link-content.example/prouct/2", 3, "Product name mock 2", "This is a product that generated to be content mock 2", 1, 42m },
                    { 3, "https://product-barcode-link-content.example/prouct/3", 4, "Product name mock 3", "This is a product that generated to be content mock 3", 1, 42m },
                    { 4, "https://product-barcode-link-content.example/prouct/4", 5, "Product name mock 4", "This is a product that generated to be content mock 4", 1, 42m },
                    { 5, "https://product-barcode-link-content.example/prouct/5", 1, "Product name mock 5", "This is a product that generated to be content mock 5", 1, 42m },
                    { 6, "https://product-barcode-link-content.example/prouct/6", 2, "Product name mock 6", "This is a product that generated to be content mock 6", 1, 42m },
                    { 7, "https://product-barcode-link-content.example/prouct/7", 3, "Product name mock 7", "This is a product that generated to be content mock 7", 1, 42m },
                    { 8, "https://product-barcode-link-content.example/prouct/8", 4, "Product name mock 8", "This is a product that generated to be content mock 8", 1, 42m },
                    { 9, "https://product-barcode-link-content.example/prouct/9", 5, "Product name mock 9", "This is a product that generated to be content mock 9", 1, 42m },
                    { 10, "https://product-barcode-link-content.example/prouct/10", 1, "Product name mock 10", "This is a product that generated to be content mock 10", 1, 42m },
                    { 11, "https://product-barcode-link-content.example/prouct/11", 2, "Product name mock 11", "This is a product that generated to be content mock 11", 1, 42m },
                    { 12, "https://product-barcode-link-content.example/prouct/12", 3, "Product name mock 12", "This is a product that generated to be content mock 12", 1, 42m },
                    { 13, "https://product-barcode-link-content.example/prouct/13", 4, "Product name mock 13", "This is a product that generated to be content mock 13", 1, 42m },
                    { 14, "https://product-barcode-link-content.example/prouct/14", 5, "Product name mock 14", "This is a product that generated to be content mock 14", 1, 42m },
                    { 15, "https://product-barcode-link-content.example/prouct/15", 1, "Product name mock 15", "This is a product that generated to be content mock 15", 1, 42m },
                    { 16, "https://product-barcode-link-content.example/prouct/16", 2, "Product name mock 16", "This is a product that generated to be content mock 16", 1, 42m },
                    { 17, "https://product-barcode-link-content.example/prouct/17", 3, "Product name mock 17", "This is a product that generated to be content mock 17", 1, 42m },
                    { 18, "https://product-barcode-link-content.example/prouct/18", 4, "Product name mock 18", "This is a product that generated to be content mock 18", 1, 42m },
                    { 19, "https://product-barcode-link-content.example/prouct/19", 5, "Product name mock 19", "This is a product that generated to be content mock 19", 1, 42m },
                    { 20, "https://product-barcode-link-content.example/prouct/20", 1, "Product name mock 20", "This is a product that generated to be content mock 20", 1, 42m },
                    { 21, "https://product-barcode-link-content.example/prouct/21", 2, "Product name mock 21", "This is a product that generated to be content mock 21", 1, 42m },
                    { 22, "https://product-barcode-link-content.example/prouct/22", 3, "Product name mock 22", "This is a product that generated to be content mock 22", 1, 42m },
                    { 23, "https://product-barcode-link-content.example/prouct/23", 4, "Product name mock 23", "This is a product that generated to be content mock 23", 1, 42m },
                    { 24, "https://product-barcode-link-content.example/prouct/24", 5, "Product name mock 24", "This is a product that generated to be content mock 24", 1, 42m },
                    { 25, "https://product-barcode-link-content.example/prouct/25", 1, "Product name mock 25", "This is a product that generated to be content mock 25", 1, 42m },
                    { 26, "https://product-barcode-link-content.example/prouct/26", 2, "Product name mock 26", "This is a product that generated to be content mock 26", 1, 42m },
                    { 27, "https://product-barcode-link-content.example/prouct/27", 3, "Product name mock 27", "This is a product that generated to be content mock 27", 1, 42m },
                    { 28, "https://product-barcode-link-content.example/prouct/28", 4, "Product name mock 28", "This is a product that generated to be content mock 28", 1, 42m },
                    { 29, "https://product-barcode-link-content.example/prouct/29", 5, "Product name mock 29", "This is a product that generated to be content mock 29", 1, 42m },
                    { 30, "https://product-barcode-link-content.example/prouct/30", 1, "Product name mock 30", "This is a product that generated to be content mock 30", 1, 42m },
                    { 31, "https://product-barcode-link-content.example/prouct/31", 2, "Product name mock 31", "This is a product that generated to be content mock 31", 1, 42m },
                    { 32, "https://product-barcode-link-content.example/prouct/32", 3, "Product name mock 32", "This is a product that generated to be content mock 32", 1, 42m },
                    { 33, "https://product-barcode-link-content.example/prouct/33", 4, "Product name mock 33", "This is a product that generated to be content mock 33", 1, 42m },
                    { 34, "https://product-barcode-link-content.example/prouct/34", 5, "Product name mock 34", "This is a product that generated to be content mock 34", 1, 42m },
                    { 35, "https://product-barcode-link-content.example/prouct/35", 1, "Product name mock 35", "This is a product that generated to be content mock 35", 1, 42m },
                    { 36, "https://product-barcode-link-content.example/prouct/36", 2, "Product name mock 36", "This is a product that generated to be content mock 36", 1, 42m },
                    { 37, "https://product-barcode-link-content.example/prouct/37", 3, "Product name mock 37", "This is a product that generated to be content mock 37", 1, 42m },
                    { 38, "https://product-barcode-link-content.example/prouct/38", 4, "Product name mock 38", "This is a product that generated to be content mock 38", 1, 42m },
                    { 39, "https://product-barcode-link-content.example/prouct/39", 5, "Product name mock 39", "This is a product that generated to be content mock 39", 1, 42m },
                    { 40, "https://product-barcode-link-content.example/prouct/40", 1, "Product name mock 40", "This is a product that generated to be content mock 40", 1, 42m },
                    { 41, "https://product-barcode-link-content.example/prouct/41", 2, "Product name mock 41", "This is a product that generated to be content mock 41", 1, 42m },
                    { 42, "https://product-barcode-link-content.example/prouct/42", 3, "Product name mock 42", "This is a product that generated to be content mock 42", 1, 42m }
                });

            migrationBuilder.InsertData(
                table: "Product",
                columns: new[] { "Id", "Barcode", "CategoryId", "Name", "ProductDescription", "StatusId", "Weight" },
                values: new object[,]
                {
                    { 43, "https://product-barcode-link-content.example/prouct/43", 4, "Product name mock 43", "This is a product that generated to be content mock 43", 1, 42m },
                    { 44, "https://product-barcode-link-content.example/prouct/44", 5, "Product name mock 44", "This is a product that generated to be content mock 44", 1, 42m },
                    { 45, "https://product-barcode-link-content.example/prouct/45", 1, "Product name mock 45", "This is a product that generated to be content mock 45", 1, 42m },
                    { 46, "https://product-barcode-link-content.example/prouct/46", 2, "Product name mock 46", "This is a product that generated to be content mock 46", 1, 42m },
                    { 47, "https://product-barcode-link-content.example/prouct/47", 3, "Product name mock 47", "This is a product that generated to be content mock 47", 1, 42m },
                    { 48, "https://product-barcode-link-content.example/prouct/48", 4, "Product name mock 48", "This is a product that generated to be content mock 48", 1, 42m },
                    { 49, "https://product-barcode-link-content.example/prouct/49", 5, "Product name mock 49", "This is a product that generated to be content mock 49", 1, 42m },
                    { 50, "https://product-barcode-link-content.example/prouct/50", 1, "Product name mock 50", "This is a product that generated to be content mock 50", 1, 42m },
                    { 51, "https://product-barcode-link-content.example/prouct/51", 2, "Product name mock 51", "This is a product that generated to be content mock 51", 1, 42m },
                    { 52, "https://product-barcode-link-content.example/prouct/52", 3, "Product name mock 52", "This is a product that generated to be content mock 52", 1, 42m },
                    { 53, "https://product-barcode-link-content.example/prouct/53", 4, "Product name mock 53", "This is a product that generated to be content mock 53", 1, 42m },
                    { 54, "https://product-barcode-link-content.example/prouct/54", 5, "Product name mock 54", "This is a product that generated to be content mock 54", 1, 42m },
                    { 55, "https://product-barcode-link-content.example/prouct/55", 1, "Product name mock 55", "This is a product that generated to be content mock 55", 1, 42m },
                    { 56, "https://product-barcode-link-content.example/prouct/56", 2, "Product name mock 56", "This is a product that generated to be content mock 56", 1, 42m },
                    { 57, "https://product-barcode-link-content.example/prouct/57", 3, "Product name mock 57", "This is a product that generated to be content mock 57", 1, 42m },
                    { 58, "https://product-barcode-link-content.example/prouct/58", 4, "Product name mock 58", "This is a product that generated to be content mock 58", 1, 42m },
                    { 59, "https://product-barcode-link-content.example/prouct/59", 5, "Product name mock 59", "This is a product that generated to be content mock 59", 1, 42m },
                    { 60, "https://product-barcode-link-content.example/prouct/60", 1, "Product name mock 60", "This is a product that generated to be content mock 60", 1, 42m },
                    { 61, "https://product-barcode-link-content.example/prouct/61", 2, "Product name mock 61", "This is a product that generated to be content mock 61", 1, 42m },
                    { 62, "https://product-barcode-link-content.example/prouct/62", 3, "Product name mock 62", "This is a product that generated to be content mock 62", 1, 42m },
                    { 63, "https://product-barcode-link-content.example/prouct/63", 4, "Product name mock 63", "This is a product that generated to be content mock 63", 1, 42m },
                    { 64, "https://product-barcode-link-content.example/prouct/64", 5, "Product name mock 64", "This is a product that generated to be content mock 64", 1, 42m },
                    { 65, "https://product-barcode-link-content.example/prouct/65", 1, "Product name mock 65", "This is a product that generated to be content mock 65", 1, 42m },
                    { 66, "https://product-barcode-link-content.example/prouct/66", 2, "Product name mock 66", "This is a product that generated to be content mock 66", 1, 42m },
                    { 67, "https://product-barcode-link-content.example/prouct/67", 3, "Product name mock 67", "This is a product that generated to be content mock 67", 1, 42m },
                    { 68, "https://product-barcode-link-content.example/prouct/68", 4, "Product name mock 68", "This is a product that generated to be content mock 68", 1, 42m },
                    { 69, "https://product-barcode-link-content.example/prouct/69", 5, "Product name mock 69", "This is a product that generated to be content mock 69", 1, 42m },
                    { 70, "https://product-barcode-link-content.example/prouct/70", 1, "Product name mock 70", "This is a product that generated to be content mock 70", 1, 42m },
                    { 71, "https://product-barcode-link-content.example/prouct/71", 2, "Product name mock 71", "This is a product that generated to be content mock 71", 1, 42m },
                    { 72, "https://product-barcode-link-content.example/prouct/72", 3, "Product name mock 72", "This is a product that generated to be content mock 72", 1, 42m },
                    { 73, "https://product-barcode-link-content.example/prouct/73", 4, "Product name mock 73", "This is a product that generated to be content mock 73", 1, 42m },
                    { 74, "https://product-barcode-link-content.example/prouct/74", 5, "Product name mock 74", "This is a product that generated to be content mock 74", 1, 42m },
                    { 75, "https://product-barcode-link-content.example/prouct/75", 1, "Product name mock 75", "This is a product that generated to be content mock 75", 1, 42m },
                    { 76, "https://product-barcode-link-content.example/prouct/76", 2, "Product name mock 76", "This is a product that generated to be content mock 76", 1, 42m },
                    { 77, "https://product-barcode-link-content.example/prouct/77", 3, "Product name mock 77", "This is a product that generated to be content mock 77", 1, 42m },
                    { 78, "https://product-barcode-link-content.example/prouct/78", 4, "Product name mock 78", "This is a product that generated to be content mock 78", 1, 42m },
                    { 79, "https://product-barcode-link-content.example/prouct/79", 5, "Product name mock 79", "This is a product that generated to be content mock 79", 1, 42m },
                    { 80, "https://product-barcode-link-content.example/prouct/80", 1, "Product name mock 80", "This is a product that generated to be content mock 80", 1, 42m },
                    { 81, "https://product-barcode-link-content.example/prouct/81", 2, "Product name mock 81", "This is a product that generated to be content mock 81", 1, 42m },
                    { 82, "https://product-barcode-link-content.example/prouct/82", 3, "Product name mock 82", "This is a product that generated to be content mock 82", 1, 42m },
                    { 83, "https://product-barcode-link-content.example/prouct/83", 4, "Product name mock 83", "This is a product that generated to be content mock 83", 1, 42m },
                    { 84, "https://product-barcode-link-content.example/prouct/84", 5, "Product name mock 84", "This is a product that generated to be content mock 84", 1, 42m }
                });

            migrationBuilder.InsertData(
                table: "Product",
                columns: new[] { "Id", "Barcode", "CategoryId", "Name", "ProductDescription", "StatusId", "Weight" },
                values: new object[,]
                {
                    { 85, "https://product-barcode-link-content.example/prouct/85", 1, "Product name mock 85", "This is a product that generated to be content mock 85", 1, 42m },
                    { 86, "https://product-barcode-link-content.example/prouct/86", 2, "Product name mock 86", "This is a product that generated to be content mock 86", 1, 42m },
                    { 87, "https://product-barcode-link-content.example/prouct/87", 3, "Product name mock 87", "This is a product that generated to be content mock 87", 1, 42m },
                    { 88, "https://product-barcode-link-content.example/prouct/88", 4, "Product name mock 88", "This is a product that generated to be content mock 88", 1, 42m },
                    { 89, "https://product-barcode-link-content.example/prouct/89", 5, "Product name mock 89", "This is a product that generated to be content mock 89", 1, 42m },
                    { 90, "https://product-barcode-link-content.example/prouct/90", 1, "Product name mock 90", "This is a product that generated to be content mock 90", 1, 42m },
                    { 91, "https://product-barcode-link-content.example/prouct/91", 2, "Product name mock 91", "This is a product that generated to be content mock 91", 1, 42m },
                    { 92, "https://product-barcode-link-content.example/prouct/92", 3, "Product name mock 92", "This is a product that generated to be content mock 92", 1, 42m },
                    { 93, "https://product-barcode-link-content.example/prouct/93", 4, "Product name mock 93", "This is a product that generated to be content mock 93", 1, 42m },
                    { 94, "https://product-barcode-link-content.example/prouct/94", 5, "Product name mock 94", "This is a product that generated to be content mock 94", 1, 42m },
                    { 95, "https://product-barcode-link-content.example/prouct/95", 1, "Product name mock 95", "This is a product that generated to be content mock 95", 1, 42m },
                    { 96, "https://product-barcode-link-content.example/prouct/96", 2, "Product name mock 96", "This is a product that generated to be content mock 96", 1, 42m },
                    { 97, "https://product-barcode-link-content.example/prouct/97", 3, "Product name mock 97", "This is a product that generated to be content mock 97", 1, 42m },
                    { 98, "https://product-barcode-link-content.example/prouct/98", 4, "Product name mock 98", "This is a product that generated to be content mock 98", 1, 42m },
                    { 99, "https://product-barcode-link-content.example/prouct/99", 5, "Product name mock 99", "This is a product that generated to be content mock 99", 1, 42m },
                    { 100, "https://product-barcode-link-content.example/prouct/100", 1, "Product name mock 100", "This is a product that generated to be content mock 100", 1, 42m }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Product_CategoryId",
                table: "Product",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Product_StatusId",
                table: "Product",
                column: "StatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Product");

            migrationBuilder.DropTable(
                name: "Category");

            migrationBuilder.DropTable(
                name: "Status");
        }
    }
}
