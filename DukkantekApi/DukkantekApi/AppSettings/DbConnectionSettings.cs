﻿using DukkantekApi.DataAccess.Databases;
using Microsoft.Extensions.Options;

namespace DukkantekApi.AppSettings
{
    public class DbConnectionSettings: IDbConnectionSettings
    {
        public DbConnectionSettings(IOptions<ConnectionStringsSection> options)
        {
            ConnectionString = options.Value.ProductsDbConnectionString;
        }

        public string ConnectionString { get; set; }
    }
}