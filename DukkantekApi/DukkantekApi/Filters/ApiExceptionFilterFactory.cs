﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace DukkantekApi.Filters
{
    public sealed class ApiExceptionFilterFactory : IFilterFactory
    {
        public bool IsReusable => false;

        public IFilterMetadata CreateInstance(IServiceProvider serviceProvider)
        {
            var logger = serviceProvider.GetRequiredService<ILogger<ApiExceptionFilterAttribute>>();

            var env = serviceProvider.GetRequiredService<IWebHostEnvironment>();

            return new ApiExceptionFilterAttribute(logger, env);
        }
    }
}