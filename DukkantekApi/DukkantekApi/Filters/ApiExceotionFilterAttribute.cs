﻿using DukkantekApi.Domain.Exceptions;
using DukkantekApi.Domain.Models.Errors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DukkantekApi.Filters
{
    public sealed class ApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<ApiExceptionFilterAttribute> _logger;
        private readonly IWebHostEnvironment _environment;

        public ApiExceptionFilterAttribute(ILogger<ApiExceptionFilterAttribute> logger, IWebHostEnvironment environment)
        {
            _logger = logger;
            _environment = environment;
        }

        public override void OnException(ExceptionContext context)
        {
            context.Result = CreateResult(context.Exception);
        }

        private ObjectResult CreateResult(Exception ex)
        {
            if (ex is DomainValidationException validationException)
            {
                var responseBody = CreateErrorResponseBody(validationException, _environment.IsDevelopment());
                
                responseBody.ValidationErrors = validationException.ValidationErrors;

                var validationResult = new ObjectResult(responseBody)
                {
                    StatusCode = 400
                };

                _logger.LogError(ex, $"Validation error has occurred; ErrorId={responseBody.ErrorGuid}", responseBody);

                return validationResult;
            }
            
            var errorBody = CreateErrorResponseBody(ex, _environment.IsDevelopment());

            var apiResult = new ObjectResult(errorBody)
            {
                StatusCode = 520
            };

            _logger.LogError(ex, $"An unknown Error has occurred; ErrorId={errorBody.ErrorGuid}", errorBody);

            return apiResult;
        }

        private static ApiResponseErrorModel CreateErrorResponseBody(Exception exception, bool showSystemErrors)
        {
            var currentException = exception;

            ApiResponseErrorModel? resultError = null;
            ApiResponseErrorModel? currentError = null;

            while (currentException != null)
            {
                var error = ExceptionToError(currentException, showSystemErrors);

                if (resultError == null)
                {
                    resultError = error;
                }

                if (currentError != null)
                {
                    currentError.InnerError = error;
                }

                currentException = currentException.InnerException;
                currentError = error;
            }

            return resultError!;
        }

        private static ApiResponseErrorModel ExceptionToError(Exception exception, bool showSystemErrors = false)
        {
            var error = new ApiResponseErrorModel
            {
                Message = exception.Message
            };

            if (!showSystemErrors)
                return error;

            var errorInfo = new ErrorSystemInfo
            {
                Type = exception.GetType().FullName ?? string.Empty,
                Message = exception.Message,
                StackTrace = exception.StackTrace ?? string.Empty
            };

            error.SystemInfo = errorInfo;

            return error;
        }
    }
}
