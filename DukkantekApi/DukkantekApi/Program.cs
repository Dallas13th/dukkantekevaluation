using DukkantekApi.AppSettings;
using DukkantekApi.DataAccess.Databases;
using DukkantekApi.DataAccess.Databases.Products;
using DukkantekApi.Domain.Services;
using DukkantekApi.Filters;
using DbConnectionSettings = DukkantekApi.AppSettings.DbConnectionSettings;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers(options =>
{
    options.Filters.Add(new ApiExceptionFilterFactory());
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Dependency injections
builder.Services.Configure<ConnectionStringsSection>(builder.Configuration.GetSection("ConnectionStrings"));
builder.Services.AddSingleton<IDbConnectionSettings, DbConnectionSettings>();
builder.Services.AddTransient<ProductsDbContextBase, ProductsSqlServerContext>();
builder.Services.AddTransient<IProductsService, ProductsService>();
builder.Services.AddTransient<IStatusService, StatusService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
