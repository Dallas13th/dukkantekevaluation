﻿using Microsoft.AspNetCore.Mvc;

namespace DukkantekApi.Controllers
{
    [Route("~/")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public sealed class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            const string relativePath = "~/swagger";

            var url = Url.Content(relativePath);

            return Redirect(url);
        }
    }
}
