﻿using DukkantekApi.Domain.Models.Products;
using DukkantekApi.Domain.Services;
using Microsoft.AspNetCore.Mvc;

namespace DukkantekApi.Controllers
{
    [Route("[controller]/[action]")]
    public class ProductsController : ControllerBase
    {
        private readonly IProductsService _productsService;
        private readonly IStatusService _statusService;
        
        public ProductsController(IProductsService productsService, IStatusService statusService)
        {
            _productsService = productsService;
            _statusService = statusService;
        }
        
        /// <summary>
        /// Get list of all products
        /// </summary>
        /// <param name="statusId">Filter by Status id</param>
        /// <returns>List of products</returns>
        [HttpGet]
        public async Task<List<ProductResultModel>> GetProductsListAsync([FromQuery] int? statusId)
        {
            return await _productsService.GetProductsListAsync(statusId);
        }

        /// <summary>
        /// Get products count
        /// </summary>
        /// <param name="statusId">Filter by Status id</param>
        /// <returns>Count as Integer value</returns>
        [HttpGet]
        public async Task<int> GetProductsCountAsync([FromQuery] int? statusId)
        {
            return await _productsService.GetProductsCountAsync(statusId);
        }

        /// <summary>
        /// Get list of all existing statuses
        /// </summary>
        /// <returns>List of statuses</returns>
        [HttpGet]
        public async Task<List<StatusResultModel>> GetProductsStatusListAsync()
        {
            return await _statusService.GetStatusesAsync();
        }

        /// <summary>
        /// Set status for specified product
        /// </summary>
        /// <param name="productId">Id of desired product</param>
        /// <param name="statusId">Id of new product status</param>
        /// <returns>Void</returns>
        [HttpPost]
        [Route("{productId:int}")]
        public async Task UpdateProductStatusAsync([FromRoute] int productId, [FromBody] int statusId)
        {
            await _productsService.SetProductStatusAsync(productId, statusId);
        }

        /// <summary>
        /// Sell product. Sets the Status sold for selected product
        /// </summary>
        /// <param name="productId">Id of desired product</param>
        /// <returns></returns>
        [HttpPost]
        [Route("{productId:int}")]
        public async Task SellProductAsync([FromRoute] int productId)
        {
            await _productsService.SellProductAsync(productId);
        }
    }
}